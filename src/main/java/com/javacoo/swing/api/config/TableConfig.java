package com.javacoo.swing.api.config;

import com.javacoo.xkernel.spi.Spi;

/**
 * 表格配置
 * <p>说明:</p>
 * <li></li>
 *
 * @author duanyong@jccfc.com
 * @date 2020/9/6 22:00
 */
@Spi("default")
public interface TableConfig {
    /**
     * 表格列宽度数组
     * <p>说明:</p>
     * <li></li>
     * @author duanyong
     * @date 2020/9/6 22:04
     */
    int[] getColumnWidths();
    /**
     *  表格标题数组
     * <p>说明:</p>
     * <li></li>
     * @author duanyong
     * @date 2020/9/6 22:08
     */
    Object[] getColumnTitles();
}
