package com.javacoo.swing.api.cache;

import com.javacoo.xkernel.spi.Spi;

import java.util.Map;

/**
 * 缓存服务
 * <p>说明:</p>
 * <li></li>
 *
 * @author DuanYong
 * @version 1.0
 * @since 2019/8/30 11:17
 */
@Spi("default")
public interface CacheService {
    /**
     *  根据缓存KEY获取值
     * <p>说明:</p>
     * <li></li>
     * @author DuanYong
     * @since 2019/8/30 11:18
     * @param key 键
     * @return String 值
     * @version 1.0
     */
    String getCache(final String key);
    /**
     *  保存缓存
     * <p>说明:</p>
     * <li></li>
     * @author DuanYong
     * @since 2019/8/30 11:19
     * @param key 键
     * @param value 值
     * @version 1.0
     */
    void putCache(final String key,final String value);
    /**
     *  删除缓存
     * <p>说明:</p>
     * <li></li>
     * @author DuanYong
     * @since 2019/8/30 11:20
     * @param key 键
     * @version 1.0
     */
    void remove(final String key);
    /**
     *  批量保存
     * <p>说明:</p>
     * <li></li>
     * @author DuanYong
     * @since 2019/8/30 12:57
     * @param values
     * @version 1.0
     */
    void putAll(final Map<String, String> values);
    /**
     *  清空所有缓存
     * <p>说明:</p>
     * <li></li>
     * @author DuanYong
     * @since 2019/8/30 13:19
     * @version 1.0
     */
    void clear();

}
