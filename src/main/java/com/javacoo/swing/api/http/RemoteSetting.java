package com.javacoo.swing.api.http;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * 远程配置
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年12月19日上午10:25:13
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class RemoteSetting {
	/** 连接池最大并发连接数,默认值*/
	public static final int DEFAULT_MAX_TOTAL = 300;
	/** 连接池最大并发连接数,默认值*/
	public static final int DEFAULT_MAX_ROUTE = 50;
	/** 数据传输处理时间,默认值*/
	public static final int DEFAULT_SOCKET_TIMEOUT = 4 * 1000;
	/** 建立连接的timeout时间,默认值*/
	public static final int DEFAULT_CONN_TIMEOUT = 3 * 1000;
	/** 从连接池中获取连接的timeout时间,默认值*/
	public static final int DEFAULT_CONN_REQ_TIMEOUT = 1 * 1000;

	//public String contentType = "application/x-www-form-urlencoded;charset=UTF-8";
	public String contentType = "application/json;charset=UTF-8";
	public String accept = "application/json,text/javascript";
	public String acceptEncoding = "gzip,deflate,br";
	public String acceptLanguage = "zh-CN,zh;q=0.8,zh-TW;q=0.7,zh-HK;q=0.5,en-US;q=0.3,en;q=0.2";
	public String userAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/55.0.2883.87 UBrowser/6.2.4094.1 Safari/537.36";
	public String acceptCharset = "ISO-8859-1,utf-8;q\\=0.7,*;q\\=0.7";
	public String keepAlive = "300";
	public String connection =  "Keep-Alive";
	public String cacheControl = "no-cache";
	public String host = "mbillexprod.alipay.com";
	public String referer = "https://mbillexprod.alipay.com/enterprise/fundAccountDetail.htm";
	
	/**连接池最大并发连接数*/
	private  int maxTotal = DEFAULT_MAX_TOTAL;
	/**单路由最大并发数*/
	private  int defaultMaxPerRoute = DEFAULT_MAX_ROUTE;
    /**数据传输处理时间*/
	private int socketTimeout = DEFAULT_SOCKET_TIMEOUT;
	/**建立连接的timeout时间*/
	private int connectionTimeout = DEFAULT_CONN_TIMEOUT;
	/**从连接池中获取连接的timeout时间*/
	private int connectionRequestTimeout = DEFAULT_CONN_REQ_TIMEOUT;
}
