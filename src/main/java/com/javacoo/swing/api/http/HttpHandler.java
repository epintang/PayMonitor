package com.javacoo.swing.api.http;

import com.javacoo.xkernel.spi.Spi;

import java.util.Map;

/**
 * Http处理接口
 * <p>说明:</p>
 * <li></li>
 * @author DuanYong
 * @since 2018年12月19日上午9:33:11
 */
@Spi("default")
public interface HttpHandler {
	/**
	 * 初始化
	 * <p>说明:</p>
	 * <li></li>
	 * @author DuanYong
	 * @param remoteSetting
	 * @since 2019年1月6日下午5:13:03
	 */
	HttpHandler init(RemoteSetting remoteSetting);
   /**
	 * 同步post请求
	 * <p>说明:</p>
	 * <li></li>
	 * @author DuanYong
     * @param address 请求地址
     * @param data 请求数据
	 * @return 响应数据
    * @since 2018年12月19日上午9:33:30
    */
	String post(String address, Map<String, Object> data, String cookie);
	/**
	 *  同步get请求
	 * <p>说明:</p>
	 * <li></li>
	 * @author DuanYong
     * @param address 请求地址
	 * @param cookie cookie
	 * @return 响应数据
	 * @since 2018年12月19日上午9:34:15
	 */
	String get(String address, String cookie);
}
