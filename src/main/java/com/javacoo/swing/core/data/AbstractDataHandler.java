package com.javacoo.swing.core.data;

import com.javacoo.swing.api.data.DataHandler;
import lombok.extern.slf4j.Slf4j;

import java.util.Optional;
import java.util.concurrent.*;

/**
 * 抽象数据处理类
 * <p>说明:</p>
 * <li></li>
 *
 * @Author DuanYong
 * @Since 2019/8/30 23:47
 * @Version 1.0
 */
@Slf4j
public abstract class AbstractDataHandler<T> implements DataHandler<String> {
    protected ExecutorService executorService = new ThreadPoolExecutor(Runtime.getRuntime().availableProcessors(), Runtime.getRuntime().availableProcessors(),
            0L, TimeUnit.MILLISECONDS,
            new LinkedBlockingQueue<Runnable>(),Executors.defaultThreadFactory());
    @Override
    public final void handle(final String data){
        readyData(data).ifPresent(this::submit);
    }
    /**
     * 提交准备好的数据
     * <p>说明:</p>
     * <li></li>
     * @author duanyong@jccfc.com
     * @date 2020/9/5 22:53
     * @Params readyData 准备好的数据
     */
    private void submit(final String readyData){
        executorService.submit(()->{
            //解析
            T t = parser(readyData);
            if(null == t){
                return;
            }
            //执行处理
            doHandle(t);
        });
    }
    /**
     * 准备数据
     * <p>说明:</p>
     * <li></li>
     * @author duanyong@jccfc.com
     * @date 2020/9/5 22:54
     * @Params data 原始数据
     */
    protected abstract Optional<String> readyData(String data);

    /**
     * 执行处理
     * <p>说明:</p>
     * <li></li>
     * @Author DuanYong
     * @Since 2019/8/30 23:57
     * @Version 1.0
     * @Params t
     */
    protected abstract void doHandle(T t);

    /**
     * 解析数据
     * <p>说明:</p>
     * <li></li>
     * @Author DuanYong
     * @Since 2019/8/30 23:55
     * @Version 1.0
     * @Params rawData 原始数据
     * @Return T
     */
    protected abstract T parser(String rawData);


}
