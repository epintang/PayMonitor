package com.javacoo.swing.core.utils;

import java.util.Random;

/**
 * 其他工具类
 * <p>说明:</p>
 * <li></li>
 *
 * @Author DuanYong
 * @Since 2019/9/1 9:51
 * @Version 1.0
 */
public class OtherUtil {
    public static int getRandom(int min, int max){
        Random random = new Random();
        int i = random.nextInt(max) % (max - min + 1) + min;
        return i;
    }
}
