package com.javacoo.swing.core.event;

/**
 * 动作类型
 * <p>说明:</p>
 * <li></li>
 *
 * @Author DuanYong
 * @Since 2019/9/1 11:09
 * @Version 1.0
 */
public enum ActionType {
    LOGIN,START,STOP,LOGIN_WIN_VISIBLE,LOGIN_WIN_HIDDEN,DETAIL_URL_LOADED,UID_DATA;
}
