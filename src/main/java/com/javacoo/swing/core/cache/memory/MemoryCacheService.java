package com.javacoo.swing.core.cache.memory;

import com.javacoo.swing.api.cache.CacheService;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 基于内存实现
 * <p>说明:</p>
 * <li></li>
 *
 * @author DuanYong
 * @version 1.0
 * @since 2019/8/30 11:24
 */
public class MemoryCacheService implements CacheService {
    /**
     * 数据MAP
     */
    private static Map<String, String> DATA_MAP = new ConcurrentHashMap<String, String>();

    @Override
    public String getCache(String key) {
        return DATA_MAP.get(key);
    }

    @Override
    public void putCache(String key, String value) {
        DATA_MAP.put(key,value);
    }

    @Override
    public void remove(String key) {
        DATA_MAP.remove(key);
    }

    @Override
    public void putAll(Map<String, String> values) {
        DATA_MAP.putAll(values);
    }

    @Override
    public void clear() {
        DATA_MAP.clear();
    }
}
